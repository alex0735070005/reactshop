var express = require('express');
var path = require('path');
var app = express();
var products = require('./data/products.json');

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '/public/index.html'));
});

app.post('/get-products', function (req, res) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.status(200);
  res.send(products);
});

var dir = path.join(__dirname, 'public');
app.use(express.static(dir));

app.listen(9000, function () {
  console.log('Example app listening on port 9000!');
});