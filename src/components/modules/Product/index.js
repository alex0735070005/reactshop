import React from 'react';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';

import './style.scss';

const Product = function(props)
{
    const {
        id,
        name,
        description,
        price,
        image,
    } = props.data;

    const add = ()=> {
        props.addProduct(props.data)
    }

    return (
        <div className="col-md-3">
           <div className = "product">
                <h3>{name}</h3>
                <div className="p-img">
                    <Link to={"/product/" + id} >
                        <img className="img-fluid" src={image} />
                    </Link>
                </div>
                <p>
                    {description}
                </p>
                <div className="d-flex justify-content-between">
                    <div className = "price">
                            {price}
                    </div>
                    <button onClick={add} className="btn btn-primary" type="button">Add cart</button>
                </div>
           </div>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {}
}
  
const mapDispatchToProps = (dispatch) => {
    return {
        addProduct:(d)=>{
            dispatch({type:'ADD_PRODUCT', data:d})
        }
    }
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Product)