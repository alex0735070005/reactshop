import React from 'react';
import { connect } from 'react-redux';

const Product = function(props)
{

    const {
        image,
        name,
        quantity,
        sum
    } = props.data;

    return (
        <tr>
            <td><img src={'/'+image} /></td>
            <td>{name}</td>
            <td>{quantity}</td>
            <td>{sum}</td>
            <td onClick={()=>{props.removeProduct(props.data)}}>X</td>
        </tr>
    )
}

const mapStateToProps = (state) => {
    return {}
}
  
const mapDispatchToProps = (dispatch) => {
    return {
        removeProduct:(d)=>{
            dispatch({type:'REMOVE_PRODUCT', data:d})
        }
    }
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Product)