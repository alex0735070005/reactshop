import React from 'react';

const Head = function()
{
    return (
        <thead>
            <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Count</th>
                <th>Sum</th>
                <th></th>
            </tr>
        </thead>
    )
}


export default Head;