import React from 'react';
import Product from './Product';

const Body = function(props)
{
    return (
        <tbody>
            {
                props.products.map((item, k)=>{
                    return <Product data = {item} key = {k} removeProduct = {props.removeProduct} />
                })
            }
        </tbody>
    )
}


export default Body;