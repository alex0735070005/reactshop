import React from 'react';
import {Link} from 'react-router-dom';
import menu from './data.json'
import './style.scss';

const ShowMenu = ()=> 
{
    let dataShow = menu.map((item, k)=>{
        return (
            <li key={k}><Link to={item.link}>{item.name}</Link></li>
        );
    });

    return dataShow;
}

const Menu = function()
{
    return (
        <ul className="nav menu">
            <ShowMenu />
        </ul>
    )
}


export default Menu;