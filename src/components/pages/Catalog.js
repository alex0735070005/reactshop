import React, {Component} from 'react';
import Product from '../modules/Product';
import { connect } from 'react-redux';

const ShowProducts = (props) => 
{
    const {products, value} = props;

    let data = [];

    for(var j in products)
    {
        let product = products[j];

        if(JSON.stringify(product).search(value) !== -1){
            data.push(<Product key = {j} data = {product} />)
        }
    }
   
    return data;
}

class Catalog extends Component
{
    constructor(props){
        super(props);

        this.state = {
            value:"",
            error:false
        }
    }

    onInput = (e) =>{
        let str = e.target.value;

        if(!/[\[\]]/.test(str)){
            this.setState({value:e.target.value, error:false})
        }else{
            this.setState({error:true})
        }
    }

    render(){
        return (
            <div id="catalog" className="mt-4">
                <div className="row mb-4">
                    <div className="col-md-12">
                        <input onInput={this.onInput} className="form-control" type="text" placeholder="Search product" />
                        {
                           this.state.error && <span>Not valid string</span>
                        }
                    </div>
                </div>
                <div className="row">
                    <ShowProducts value={this.state.value} products = {this.props.products}/>
                </div>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
      products: state.catalog.products
    }
}
  
const mapDispatchToProps = (dispatch) => {
    return {}
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Catalog)