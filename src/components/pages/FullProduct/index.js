import React from 'react';
import { connect } from 'react-redux';

import './style.scss';

const getProductById = (products, id) => {
  
    for(var j in products){
        if(+products[j]['id'] === +id) return products[j];
    }
    return null;
}

const FullProduct = function(props)
{

    const id = props.match.params.slug;

    let data = getProductById(props.products, id);

    const {
        name,
        fullDescription,
        price,
        image,
    } = data

    const add = ()=> {
        props.addProduct(data)
    }

    return (
        <div className = "product row mt-4 ml-0 mr-0">
           
            <div className="p-img col-md-4">
                <img className="img-fluid" src={'/'+image} />
            </div>
            <div className="col-md-8">
                <h3 className="text-left">{name}</h3>
                <p>
                    {fullDescription}
                </p>
                <div className="d-flex justify-content-between">
                    <div className = "price">
                            Price: {price}
                    </div>
                    <button onClick={add} className="btn btn-primary" type="button">Add cart</button>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        products:state.catalog.products
    }
}
  
const mapDispatchToProps = (dispatch) => {
    return {
        addProduct:(d)=>{
            dispatch({type:'ADD_PRODUCT', data:d})
        }
    }
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FullProduct)