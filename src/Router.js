import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home     from './components/pages/Home';
import Catalog  from './components/pages/Catalog';
import Gallery  from './components/pages/Gallery';
import FullProduct  from './components/pages/FullProduct';

const Router = function(props){
   
    return (
        <Switch>
            <Route exact path='/' component={Home}/>
            <Route path='/catalog' component={Catalog}/>
            <Route path='/product/:slug' component={FullProduct}/>
            <Route path='/gallery' component={Gallery}/>
        </Switch>
    )
}


export default Router;