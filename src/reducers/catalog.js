
import products from '../../data/products.json';

const initialState = {
  products:products
}


const catalog = (state = initialState, action) => {
    
    switch (action.type) {

      case 'ADD_TODO':
        return {
          id: action.id,
          text: action.text,
          completed: false
        }
      
  
      default:
        return state
    }
}

export default catalog;