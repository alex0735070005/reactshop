import React, {Component} from 'react';
import Header from  './components/static/Header';
import Footer from  './components/static/Footer';
import Router from  './Router';
import { BrowserRouter } from 'react-router-dom';

class App extends Component {

    render()
    {
        return ( 
            <BrowserRouter>
                <div className="container">
                    <Header />
                    <Router />
                    <Footer />
                </div>
            </BrowserRouter>
        )
    }
}


export default App;